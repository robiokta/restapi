<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
    }
    function pencarian(){
        $query_search =$this->input->get('query');
        if ($query_search=='') {
            $query['hasil_cari_submenu'] = $this->db->get("sub_menu")->result();
            $query['status']='201';
        }
        else{
            $this->db->like('nm_sub_menu', $query_search);
            $query['hasil_cari_submenu']= $this->db->get("sub_menu")->result();
            $query['status']='200';
        }
        echo json_encode($query);
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
    }
    function berita_tampil(){
        $jenis =$this->input->get('jenis');
        $start =$this->input->get('start');
        $jenis1=$this->base65($jenis);
        $start1=$this->base65($start);
        
        // die();
         if ($jenis=='') {
            $this->db->limit(3);
            $query['hasil_berita'] = $this->db->get("berita")->result();
        }
        else if($jenis=='semua'&& $start=='') {
            $this->db->limit(5);
            $query['hasil_berita_semua']= $this->db->get("berita")->result();
        }
        elseif ($jenis1=='semua'&& $start1!='') {
           
            $this->db->limit(5,$start1);
            $query['hasil_berita_semua']= $this->db->get("berita")->result();
        }
        echo json_encode($query);
    }
    function base65($i_data){
        $i_data=base64_decode($i_data);        
            $i_data=substr($i_data,0,1).substr($i_data,2,120000);        
            $i_data=base64_decode($i_data);
            $i_data=substr($i_data,0,5).substr($i_data,6,120000);
            $i_data=base64_decode($i_data);
            $i_data=substr($i_data,0,1).substr($i_data,2,120000);
            $i_data=base64_decode($i_data);
            $i_data=substr($i_data,0,5).substr($i_data,6,120000);
            $i_data=base64_decode($i_data);
            $i_data=substr($i_data,0,1).substr($i_data,2,120000);
            $i_data=base64_decode($i_data);
            return $i_data;
    }
}
